//
//  MusicMapViewController.m
//  MusicMap
//
//  Created by Glenn Thrope on 1/31/15.
//  Copyright (c) 2015 Thrope Inc. All rights reserved.
//

#import "MusicMapViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreLocation/CoreLocation.h>

@interface MusicMapViewController () <AVAudioRecorderDelegate, AVAudioPlayerDelegate, CLLocationManagerDelegate> {
    AVAudioRecorder *recorder;
    AVAudioPlayer *player;
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
}

@property (nonatomic, weak) IBOutlet UILabel *status;
@property (nonatomic, weak) IBOutlet UIButton *recordButton;
@property (nonatomic, weak) IBOutlet UIButton *playbackButton;

@property (nonatomic, weak) IBOutlet UILabel *latitude;
@property (nonatomic, weak) IBOutlet UILabel *longitude;
@property (nonatomic, weak) IBOutlet UILabel *address;

@end

@implementation MusicMapViewController

#pragma mark - UIViewController overrides

-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    // configure audioSession
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionMixWithOthers error:nil];
    [audioSession setActive:YES error:nil];
    
    /* instantiate recorder, locationManager, geocoder */
    
    // define output URL for recording
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               @"MyAudioMemo.m4a",
                               nil];
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    
    // Define the recorder settings
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    // create recorder
    recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:nil];
    recorder.delegate = self;
    recorder.meteringEnabled = YES;

    // create location manager
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    // create geocoder
    geocoder = [[CLGeocoder alloc] init];
    
    /* request permissions */
    
    // request permissions to get location
    [locationManager requestAlwaysAuthorization];
    
    // request record permission
    [audioSession requestRecordPermission:^(BOOL granted) {
        NSLog(@"permission : %d", granted);
    }];
}

#pragma mark - IBActions

-(IBAction)record {
    
    /*
    if(self.isPlayingBack) {
        [self stopPlayback];
        self.isPlayingBack = false;
    }*/
    
    if(!recorder.isRecording) {
        [recorder record];
        
        [self.recordButton setTitle:@"Stop recording" forState:UIControlStateNormal];
        self.playbackButton.enabled = false;
        self.status.text = [NSString stringWithFormat:@"Recording"];
    }
    else {
        [recorder stop];
        
        [self.recordButton setTitle:@"Start recording" forState:UIControlStateNormal];
        self.playbackButton.enabled = true;
        self.status.text = @"Ready";
    }
}

-(IBAction)playback {
    
    /*
    if(self.isRecording) {
        [self stopRecording];
        self.isRecording = false;
    } */
    
    if(!player.isPlaying) {
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:recorder.url error:nil];
        player.delegate = self;
        [player play];

        self.recordButton.enabled = false;
        [self.playbackButton setTitle:@"Stop playback" forState:UIControlStateNormal];
        self.status.text = [NSString stringWithFormat:@"Playing back"];
    }
    else {
        [player stop];

        self.recordButton.enabled = true;
        [self.playbackButton setTitle:@"Start playback" forState:UIControlStateNormal];
        self.status.text = @"Ready";
    }
}

-(IBAction)getLocation {
    
    [locationManager startUpdatingLocation];
    
    self.latitude.text = @"<None>";
    self.longitude.text = @"<None>";
    self.address.text = @"<None>";
}

#pragma mark - Delegate methods

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    
    self.recordButton.enabled = true;
    [self.playbackButton setTitle:@"Start playback" forState:UIControlStateNormal];
    self.status.text = @"Ready";
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {

    [manager stopUpdatingLocation];

    CLLocation* location = [locations lastObject];
    self.latitude.text = [NSString stringWithFormat:@"%+.6f", location.coordinate.latitude];
    self.longitude.text = [NSString stringWithFormat:@"%+.6f", location.coordinate.longitude];
    
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error == nil && [placemarks count] > 0) {
            CLPlacemark *placemark = [placemarks lastObject];
            self.address.text = [NSString stringWithFormat:@"%@ %@\n%@ %@\n%@\n%@",
                                 placemark.subThoroughfare, placemark.thoroughfare,
                                 placemark.postalCode, placemark.locality,
                                 placemark.administrativeArea,
                                 placemark.country];
        } else {
            NSLog(@"%@", error.debugDescription);
        }
    }];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

@end
